# digdag-server
Docker sample of digdag-server

# Getting started

## Installation

Clone the repository

    PS> git clone https://d_suke@bitbucket.org/d_suke/digdag-server.git

Change to the project folder

    PS> cd digdag-server

start container use docker-compose

    PS> docker-compose up

digdag container login

    PS> docker-compose exec digdag bash
    root@ae103f9f5fef:/#

[digdag command]
https://techium.hatenablog.com/entry/2016/06/20/090000


postgresql connect

    root@ae103f9f5fef:/#psql -h postgresql -U digdag
    
